export enum Plan {
  SCOUT = "Scout",
  EXPLORER = "Explorer",
  SECRET_AGENT = "Secret Agent",
}
