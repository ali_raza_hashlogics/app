export enum CheckoutMode {
  SUBSCRIPTION = "subscription",
  PAYMENT = "payment",
}
