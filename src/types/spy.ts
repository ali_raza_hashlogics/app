export interface SpyType {
  title: string;
  key: string;
  tableName: string;
  permissions: {
    "1d": string[];
    "3d": string[];
    "7d": string[];
    "4w": string[];
  };
  children?: SpyType[];
  iframe: boolean;
}
