export interface FiltersType {
    blockchain: string;
    token_symbol: string;
    net_flow_usd_from: string;
    net_flow_usd_to: string;
    sell_amount_usd_from: string;
    sell_amount_usd_to: string;
    buy_amount_usd_from: string;
    buy_amount_usd_to: string;
    sellers: string;
    buyers: string;
  }