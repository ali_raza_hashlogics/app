export type Toast = "success" | "info" | "warning" | "error";
