export type Response = {
  message: string;
  error: boolean;
  data: object;
};
