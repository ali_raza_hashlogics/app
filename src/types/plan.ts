export interface Price {
  price: string | number;
  priceId: string;
  interval: string;
  status: string;
}

export interface Plan {
  name: string;
  description: string;
  img: string;
  productId: string;
  status: string;
  offerings: string[];
  activePrice: Price | null;
  prices: Price[];
}
