export type Insider = {
  blockchain: string;
  token_symbol: string;
  net_flow_usd: number;
  buy_amount_usd: number;
  sell_amount_usd: number;
  buyers: number;
  sellers: number;
  "% of Spies transacted": number; // Renamed to follow naming conventions
  token_address: string;
  chartiFrame: string; // Contains HTML iframe tag as a string
  index: number;
  "Market Cap": number
};
