export interface Subscription {
  subscriptionId: string;
  userId: string;
  status: string;
}
