import { ReactNode } from "react";
import "./styles.scss";

const AuthLayout = ({ children }: { children: ReactNode }) => {
  return <div className="auth-layout">{children}</div>;
};

export default AuthLayout;
