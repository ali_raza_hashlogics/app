import * as yup from "yup";
import { emailValidationSchema, passwordValidationSchema } from "./commonSchemas";

export const loginFormValidationSchema = yup.object({
  email: emailValidationSchema,
  password: passwordValidationSchema,
});
