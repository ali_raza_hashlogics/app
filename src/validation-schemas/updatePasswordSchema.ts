import * as yup from "yup";
import { passwordValidationSchema } from "./commonSchemas";

export const updatePasswordFormValidationSchema = yup.object({
  currentPassword: passwordValidationSchema,
  newPassword: passwordValidationSchema,
});
