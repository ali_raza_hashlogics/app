import * as yup from "yup";
import { emailValidationSchema } from "./commonSchemas";

export const updateEmailFormValidationSchema = yup.object({
  email: emailValidationSchema,
});
