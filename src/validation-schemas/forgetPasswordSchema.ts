import * as yup from "yup";
import { emailValidationSchema } from "./commonSchemas";

export const forgetPasswordFormValidationSchema = yup.object({
  email: emailValidationSchema,
  agreeToTerms: yup.boolean().oneOf([true], "You must agree to the terms"),
});
