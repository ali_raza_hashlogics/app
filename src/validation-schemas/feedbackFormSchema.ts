import * as yup from "yup";
import { emailValidationSchema } from "./commonSchemas";

export const feedbackFormSchema = yup.object({
  name: yup.string().required("Please provide your  name."),
  email: emailValidationSchema,
  description: yup.string().required("Please provide your feedback."),
});
