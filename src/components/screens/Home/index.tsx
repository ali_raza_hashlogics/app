import FeaturesSection from "./FeaturesSection";
import FactsSection from "./FactsSection";
import HomeHeroSection from "./HomeHeroSection";

export { FeaturesSection, FactsSection, HomeHeroSection };
