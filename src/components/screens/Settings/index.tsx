import EmailSettings from "./EmailSettings";
import NotificationsSettings from "./NotificationsSettings";
import PaymentSettings from "./PaymentSettings";
import PasswordSettings from "./PasswordSettings";

export { EmailSettings, NotificationsSettings, PaymentSettings, PasswordSettings };
