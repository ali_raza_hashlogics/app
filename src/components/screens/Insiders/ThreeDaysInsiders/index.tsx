import { Flex, PaginationProps, Spin } from "antd";
import { DashboardContentSection } from "../../../common";
import InsidersTable from "../InsidersTable";
import { useCallback, useEffect, useLayoutEffect, useMemo, useState } from "react";
import { useLazyGetThreeDaysInsidersQuery } from "../../../../services/insider";
import InsidersNoDataMessage from "../InsidersNoDataMessage";
import { Pagination } from "../../../ui-elements";
import { nftTradedColumns, tokensTradedColumns } from "../../../../constants";
import { useParams } from "react-router-dom";
import { useAuth, useUser } from "@clerk/clerk-react";
import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { useToast } from "../../../../hooks";
import { Response, SpyType, FiltersType } from "../../../../types";
import { RootState } from "../../../../slices";
import { connect } from "react-redux";
import InsidersUpgradeOverlay from "../InsidersUpgradeOverlay";
import { filtersInitialState } from "../../../../constants/filter";

interface ThreeDaysInsidersProps {
  selectedSpy: SpyType;
}

const ThreeDaysInsiders = ({ selectedSpy }: ThreeDaysInsidersProps) => {
  const [filters, setFilters] = useState(filtersInitialState);
  const { user } = useUser();
  const { errorToast } = useToast();
  const { getToken, signOut } = useAuth();
  const [currentPage, setCurrentPage] = useState(1);
  const { tableName } = useParams();
  const [trigger, result] = useLazyGetThreeDaysInsidersQuery();
  const { data, isLoading, isError, error } = result;
  const [loading, setIsLoading] = useState(false);

  const totalCount = useMemo(() => {
    if (data?.data?.totalCount) {
      return data?.data?.totalCount;
    } else return 0;
  }, [data]);

  const onPaginationChange: PaginationProps["onChange"] = page => {
    setCurrentPage(page);
  };
  const setLoading = (loading: boolean) => {
    setIsLoading(loading);
  };
  const fetchToken = useCallback(async () => {
    const token = await getToken();
    if (token && checkPermission("3d")) {
      setLoading(true);
      trigger({
        filters: JSON.stringify(filters),
        page: currentPage,
        tableName,
        token,
      }).then(() => setLoading(false));
    }
  }, [tableName, currentPage, selectedSpy, user, filters]);

  const checkPermission = useCallback(
    (day: string) => {
      if (selectedSpy && Object.keys(selectedSpy).length) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return selectedSpy?.permissions[day]?.includes(user?.publicMetadata?.planType as string);
      } else return false;
    },
    [selectedSpy, user],
  );

  const showIframe = useMemo(() => {
    if (selectedSpy && Object.keys(selectedSpy).length) return selectedSpy?.iframe;
    else if (tableName?.includes("NFT")) return false;
    else return true;
  }, [selectedSpy]);

  const handleFiltersChange = (newFilters: FiltersType) => {
    setFilters(newFilters);
  };

  useEffect(() => {
    if (isError && error) {
      const fetchBaseQueryError = error as FetchBaseQueryError;
      if (fetchBaseQueryError.data) {
        const { message } = fetchBaseQueryError.data as Response;
        errorToast("Error", message || "Something went wrong");
      } else {
        errorToast("Error", "Something went wrong");
      }
      if (fetchBaseQueryError.status === 401) signOut();
    }
  }, [isError, error]);

  useLayoutEffect(() => {
    fetchToken();
  }, [tableName, currentPage, selectedSpy, user, filters]);

  return (
    <DashboardContentSection title="Last Three Days">
      {checkPermission("3d") ? (
        isLoading || loading ? (
          <Spin />
        ) : Array.isArray(data?.data.data) && data?.data.data.length ? (
          <>
            <InsidersTable
              dataSource={data?.data.data || []}
              dataCount={totalCount}
              currentPage={currentPage}
              columns={showIframe ? tokensTradedColumns : nftTradedColumns}
              setCurrentPage={setCurrentPage}
              loading={isLoading}
              onFiltersChange={handleFiltersChange}
            />
            <Flex justify="center">
              <Pagination
                total={totalCount}
                current={currentPage}
                showSizeChanger={false}
                showPrevNextJumpers={true}
                onChange={onPaginationChange}
              />
            </Flex>
          </>
        ) : (
          <InsidersNoDataMessage />
        )
      ) : (
        <InsidersUpgradeOverlay title="3-d" />
      )}
    </DashboardContentSection>
  );
};

const mapStateToProps = (state: RootState) => {
  return {
    selectedSpy: state.spies.selectedSpy as SpyType,
  };
};

export default connect(mapStateToProps)(ThreeDaysInsiders);
