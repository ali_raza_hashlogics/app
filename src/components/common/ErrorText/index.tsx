interface ErrorTextProps {
  text: string;
}

const ErrorText = ({ text }: ErrorTextProps) => {
  return <p className="error-text">{text}</p>;
};

export default ErrorText;
