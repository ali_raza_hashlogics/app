import { Switch as AntdSwitch, SwitchProps } from "antd";
import "./styles.scss";

const Switch = (props: SwitchProps) => {
  return <AntdSwitch className="custom-switch" {...props} />;
};

export default Switch;
