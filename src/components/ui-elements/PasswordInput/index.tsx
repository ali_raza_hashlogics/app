import { Input, InputProps } from "antd";
import "./styles.scss";

const PasswordInput = (props: InputProps) => {
  const { className, ...passwordInputProps } = props;
  return <Input.Password className={`custom-password-input ${className}`} {...passwordInputProps} />;
};

export default PasswordInput;
