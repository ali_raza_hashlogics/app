import { Pagination as AntdPagination, PaginationProps } from "antd";
import "./styles.scss";

const Pagination = (props: PaginationProps) => {
  return <AntdPagination className="custom-pagination" {...props} />;
};

export default Pagination;
