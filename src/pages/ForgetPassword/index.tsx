import { Flex } from "antd";
import authImg from "../../assets/images/auth-bg-img.png";
import ForgetPasswordForm from "../../components/screens/ForgetPassword/ForgetPasswordForm";

const ForgetPassword = () => {
  return (
    <Flex justify="space-between" gap={40}>
      <ForgetPasswordForm />
      <Flex className="auth-bg-img" vertical justify="center">
        <img src={authImg} />
      </Flex>
    </Flex>
  );
};

export default ForgetPassword;
