import { Flex } from "antd";
import DashboardContentSection from "../../components/common/DashboardContentSection";
import "./styles.scss";
import { Pagination } from "../../components/ui-elements";

const Learn = () => {
  return (
    <DashboardContentSection title="Explore Educational Resources and Tutorials">
      <Flex vertical gap={8}>
        {[0, 0, 0, 0, 0, 0].map((_, index) => (
          <Flex key={index} className="learn-content-card" vertical gap={5}>
            <p className="learn-content-card-title">Getting Started with Blockchain</p>
            <p className="learn-content-card-description">
              Dive into the fundamentals of blockchain technology. Learn how it works, its applications, and its potential to disrupt
              industries.
            </p>
          </Flex>
        ))}
      </Flex>
      <Flex justify="center">
        <Pagination total={50} />
      </Flex>
    </DashboardContentSection>
  );
};

export default Learn;
